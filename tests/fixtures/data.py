import pytest
import pandas as pd

from pathlib import Path


@pytest.fixture
def pipeline_params():
    """Path to file with 7 day-old canola cylinder pipeline parameters."""
    return Path("tests") / "data" / "input" / "pipeline_params.json"


@pytest.fixture
def pipeline_chooser():
    """Dataframe with standard pipeline chooser table."""
    return pd.read_csv(Path("tests") / "data" / "input" / "pipeline_chooser_table.csv")


@pytest.fixture
def input_dir():
    """Path to input directory."""
    return Path("tests") / "data" / "input"
