import pytest
import pandas as pd


from sleap_roots import (
    DicotPipeline,
    YoungerMonocotPipeline,
    OlderMonocotPipeline,
    MultipleDicotPipeline,
)
from src.pipeline_chooser import (
    get_species,
    get_age,
    get_mode,
    get_pipeline_class,
    determine_pipeline_class,
    get_params,
)


@pytest.fixture
def classes_from_sleap_roots():
    pipeline_classes = {
        "DicotPipeline": DicotPipeline,
        "YoungerMonocotPipeline": YoungerMonocotPipeline,
        "OlderMonocotPipeline": OlderMonocotPipeline,
        "MultipleDicotPipeline": MultipleDicotPipeline,
    }
    return pipeline_classes


@pytest.fixture
def canola_pipeline_params():
    return {
        "species": "canola",
        "mode": "cylinder",
        "age": "7",
        "pipeline_class": None,
    }


@pytest.fixture
def valid_pipeline_class_present():
    """Pipeline class present in the pipeline parameters."""
    params = {
        "species": None,
        "mode": None,
        "age": None,
        "pipeline_class": "DicotPipeline",
    }
    return params


@pytest.fixture
def valid_pipeline_class_and_dataset_params_present():
    """Pipeline class and dataset parameters present in the pipeline parameters."""
    params = {
        "species": "canola",
        "mode": "cylinder",
        "age": "7",
        "pipeline_class": "DicotPipeline",
    }
    return params


@pytest.fixture
def invalid_pipeline_class_and_dataset_params_present():
    """Invalid pipeline class and dataset parameters present in the pipeline parameters."""
    params = {
        "species": "canola",
        "mode": "cylinder",
        "age": "7",
        "pipeline_class": "123456",
    }
    return params


@pytest.fixture
def real_valid_species(pipeline_chooser):
    """Valid species list in the pipeline parameters."""
    valid_species = pipeline_chooser["species"].unique().tolist()
    return valid_species


@pytest.fixture
def real_valid_modes(pipeline_chooser):
    """Valid mode list in the pipeline parameters."""
    valid_modes = pipeline_chooser["mode"].unique().tolist()
    return valid_modes


@pytest.fixture
def real_valid_ages(pipeline_chooser):
    """Valid age list in the pipeline parameters."""
    valid_ages = pipeline_chooser["age"].unique().tolist()
    return valid_ages


@pytest.fixture
def dummy_valid_species():
    return ["species1", "species2"]


@pytest.fixture
def dummy_valid_mode():
    return ["mode1", "mode2"]


@pytest.fixture
def dummy_valid_age():
    return ["young", "mature"]


@pytest.fixture
def pipeline_chooser_real_df():
    data = {
        "species": [
            "soybean",
            "canola",
            "pennycress",
            "arabidopsis",
            "arabidopsis",
            "arabidopsis",
            "rice",
            "rice",
        ],
        "mode": [
            "cylinder",
            "cylinder",
            "cylinder",
            "multiplant cylinder",
            "cylinder",
            "plate",
            "cylinder",
            "cylinder",
        ],
        "age": [
            "2, 3, 4, 5, 6, 7, 8",
            "2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13",
            "2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14",
            "2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14",
            "2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14",
            "7, 8, 9, 10, 11, 12, 13, 14",
            "2, 3, 4, 5",
            "6, 7, 8, 9, 10",
        ],
        "pipeline_class": [
            "DicotPipeline",
            "DicotPipeline",
            "DicotPipeline",
            "MultipleDicotPipeline",
            "DicotPipeline",
            "MultipleDicotPlatePipeline",
            "YoungerMonocotPipeline",
            "OlderMonocotPipeline",
        ],
    }
    return pd.DataFrame(data)


@pytest.fixture
def dataset_params_younger_monocot():
    data = {
        "species": "rice",
        "mode": "cylinder",
        "age": "3",
        "lateral_pipeline": None,
        "pipeline_class": None,
        "crown_pipeline": None,
    }
    return data


def test_get_params_valid_file(input_dir, canola_pipeline_params):
    result = get_params(input_dir)
    expected_result = canola_pipeline_params
    assert (
        result == expected_result
    ), "The function should correctly read and return the JSON data."


def test_get_params_invalid_file():
    with pytest.raises(FileNotFoundError):
        get_params("invalid_input_dir")


def test_choose_species(input_dir, real_valid_species):
    params = get_params(input_dir)
    choosen_species = get_species(params, real_valid_species)
    assert choosen_species == "canola", "The function should return the species."


def test_get_species_valid(dummy_valid_species):
    params = {"species": "species1"}
    assert get_species(params, dummy_valid_species) == "species1"


def test_get_species_invalid(dummy_valid_species):
    params = {"species": "unknown"}
    with pytest.raises(ValueError):
        get_species(params, dummy_valid_species)


def test_get_mode_valid(dummy_valid_mode):
    params = {"mode": "mode1"}
    assert get_mode(params, dummy_valid_mode) == "mode1"


def test_get_mode_invalid(dummy_valid_mode):
    params = {"mode": "unknown"}
    with pytest.raises(ValueError):
        get_mode(params, dummy_valid_mode)


def test_get_age_valid(dummy_valid_age):
    params = {"age": "young"}
    assert get_age(params, dummy_valid_age) == "young"


def test_get_age_invalid(dummy_valid_age):
    params = {"age": "old"}
    with pytest.raises(ValueError):
        get_age(params, dummy_valid_age)


@pytest.mark.parametrize(
    "params, valid_age_ranges, expected",
    [
        ({"age": "25"}, ["20-30"], "25"),
        ({"age": "17"}, ["15-18"], "17"),
        ({"age": "5"}, ["1-10"], "5"),
        ({"age": "65"}, ["60-70", "72", "75-80"], "65"),
        ({"age": "72"}, ["60-70", "72", "75-80"], "72"),
        ({"age": "30"}, ["25,26,27", "28-32"], "30"),
        ({"age": "26"}, ["25,26,27", "28-32"], "26"),
        ({"age": "28"}, ["25,26,27", "28-32"], "28"),
    ],
)
def test_get_age_valid_comprehensive(params, valid_age_ranges, expected):
    """Test get_age with valid age inputs comprehensively."""
    assert get_age(params, valid_age_ranges) == expected


@pytest.mark.parametrize(
    "params, valid_age_ranges",
    [
        ({"age": "33"}, ["20-30"]),
        ({"age": "14"}, ["15-18"]),
        ({"age": "11"}, ["1-10"]),
        ({"age": "73"}, ["60-70", "72", "75-80"]),
        ({"age": "81"}, ["60-70", "72", "75-80"]),
        ({"age": "24"}, ["25,26,27", "28-32"]),
        ({"age": "33"}, ["25,26,27", "28-32"]),
        ({"age": "0"}, ["1-10"]),
        ({"age": "100"}, ["90-99"]),
    ],
)
def test_get_age_invalid_comprehensive(params, valid_age_ranges):
    """Test get_age with invalid age inputs comprehensively."""
    with pytest.raises(ValueError):
        get_age(params, valid_age_ranges)


@pytest.mark.parametrize(
    "params, valid_age_ranges",
    [
        (
            {"age": "20"},
            ["20-20"],
        ),  # Edge case where start and end of range are the same
        ({"age": "20"}, ["20"]),  # Single age
        ({"age": "20"}, ["19,20,21"]),  # Comma-separated ages including the target
    ],
)
def test_get_age_edge_cases_comprehensive(params, valid_age_ranges):
    """Test get_age with edge cases comprehensively."""
    assert get_age(params, valid_age_ranges) == params["age"]


# Valid scenario: species="rice", mode="cylinder", age="3"
def test_get_age_valid_params(dataset_params_younger_monocot, pipeline_chooser_real_df):
    # get valid species, mode, and age ranges
    valid_species = pipeline_chooser_real_df["species"].unique().tolist()
    valid_mode = pipeline_chooser_real_df["mode"].unique().tolist()
    valid_age_ranges = pipeline_chooser_real_df["age"].unique().tolist()
    valid_pipeline_classs = pipeline_chooser_real_df["pipeline_class"].unique().tolist()

    species = get_species(dataset_params_younger_monocot, valid_species)
    mode = get_mode(dataset_params_younger_monocot, valid_mode)
    age = get_age(dataset_params_younger_monocot, valid_age_ranges)
    pipeline_class = get_pipeline_class(dataset_params_younger_monocot)
    expected_pipeline = YoungerMonocotPipeline
    assert (
        determine_pipeline_class(
            pipeline_chooser_real_df, species, mode, age, pipeline_class
        )
        == expected_pipeline
    )


# Valid scenario: Matching species, mode, and age
def test_valid_pipeline_class_real(pipeline_chooser_real_df):
    # Example for a valid combination
    species = "soybean"
    mode = "cylinder"
    age = "3"  # Age within the range specified in the fixture
    pipeline_class = None
    expected_pipeline = DicotPipeline
    assert (
        determine_pipeline_class(
            pipeline_chooser_real_df, species, mode, age, pipeline_class
        )
        == expected_pipeline
    )


# Edge case: Species with different pipelines for different ages
def test_edge_case_pipeline_class_for_different_ages(pipeline_chooser_real_df):
    species = "rice"
    mode = "cylinder"
    young_age = "4"
    older_age = "9"
    young_pipeline_class = None
    older_pipeline_class = None
    young_expected = YoungerMonocotPipeline
    older_expected = OlderMonocotPipeline

    assert (
        determine_pipeline_class(
            pipeline_chooser_real_df, species, mode, young_age, young_pipeline_class
        )
        == young_expected
    )
    assert (
        determine_pipeline_class(
            pipeline_chooser_real_df, species, mode, older_age, older_pipeline_class
        )
        == older_expected
    )


# Invalid scenario: Parameters do not match any pipelines
def test_invalid_pipeline_class_real(pipeline_chooser_real_df):
    species = "unknown"
    mode = "unknown"
    age = "100"  # Unreasonably high age, not expected to match
    pipeline_class = None
    with pytest.raises(ValueError, match="No valid pipelines found."):
        determine_pipeline_class(
            pipeline_chooser_real_df, species, mode, age, pipeline_class
        )


# Scenario: Valid species and mode but age out of range
def test_valid_species_mode_but_age_out_of_range(pipeline_chooser_real_df):
    species = "arabidopsis"
    mode = "plate"
    age = "6"  # Age not within the specified range for arabidopsis in plate mode
    pipeline_class = None
    with pytest.raises(ValueError, match="No valid pipelines found."):
        determine_pipeline_class(
            pipeline_chooser_real_df, species, mode, age, pipeline_class
        )


# Invalid scenario for lateral pipeline path
def test_invalid_params_pipeline(pipeline_chooser_real_df):
    species = "unknown"
    mode = "unknown"
    age = "100"
    pipeline_class = None
    with pytest.raises(ValueError, match="No valid pipelines found."):
        determine_pipeline_class(
            pipeline_chooser_real_df, species, mode, age, pipeline_class
        )


def test_choose_species_invalid(real_valid_species):
    with pytest.raises(ValueError):
        params = {"species": "species1"}
        get_species(params, real_valid_species)


def test_choose_mode(input_dir, real_valid_modes):
    params = get_params(input_dir)
    choosen_mode = get_mode(params, real_valid_modes)
    assert choosen_mode == "cylinder", "The function should return the mode."


def test_choose_mode_invalid(real_valid_modes):
    params = {"mode": "invalid_mode"}
    with pytest.raises(ValueError):
        get_mode(params, real_valid_modes)


def test_choose_age(input_dir, real_valid_ages):
    params = get_params(input_dir)
    choosen_age = get_age(params, real_valid_ages)
    assert choosen_age == "7", "The function should return the age."


def test_choose_age_invalid(real_valid_ages):
    params = {"age": "invalid_age"}
    with pytest.raises(ValueError):
        get_age(params, real_valid_ages)


def test_determine_pipeline_class_both_present(
    valid_pipeline_class_and_dataset_params_present,
    pipeline_chooser_real_df,
    classes_from_sleap_roots,
):
    params = valid_pipeline_class_and_dataset_params_present
    pipeline_chooser = pipeline_chooser_real_df

    # Fetch species, mode, age
    species = get_species(params, pipeline_chooser["species"].unique().tolist())
    mode = get_mode(params, pipeline_chooser["mode"].unique().tolist())
    age = get_age(params, pipeline_chooser["age"].unique().tolist())
    # Fetch pipeline class from params and sleap-roots classes
    pipeline_class = get_pipeline_class(params)

    # Determine the pipeline class using all of the params
    found_pipeline_class = determine_pipeline_class(
        pipeline_chooser, species, mode, age, pipeline_class
    )
    assert found_pipeline_class == classes_from_sleap_roots.get(
        pipeline_class
    ), "The function should return the `DicotPipeline` from sleap-roots."


def test_determine_pipeline_class_invalid(
    invalid_pipeline_class_and_dataset_params_present,
    pipeline_chooser_real_df,
):
    params = invalid_pipeline_class_and_dataset_params_present
    pipeline_chooser = pipeline_chooser_real_df

    # Fetch species, mode, age without assuming they raise an error
    species = get_species(params, pipeline_chooser["species"].unique().tolist())
    mode = get_mode(params, pipeline_chooser["mode"].unique().tolist())
    age = get_age(params, pipeline_chooser["age"].unique().tolist())

    # Fetch pipeline class
    pipeline_class = get_pipeline_class(params)

    # Try to determine pipeline class when the pipeline class is invalid
    with pytest.raises(ValueError):
        determine_pipeline_class(pipeline_chooser, species, mode, age, pipeline_class)
