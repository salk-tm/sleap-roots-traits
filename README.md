# sleap-roots-traits


## Description
The `main` function (found in "src/main.py") implements logic to get root traits for predictions of roots using `sleap-roots`. Refer to the [pipeline chooser table, verified 2024-05-10](https://salkinstitute.box.com/s/os57g6jltm4hgecr2yoeewdr25iujd9o) in the folder to see which {species, mode, age} combinations and pipelines are available.

## Arguments

- **`input_dir`**: The directory containing the necessary input files.
    - `predictions.csv`:
    - **.slp predictions for each `Series`**:
    - **`pipeline_params.json`**: This file contains all the necessary parameters to select the appropriate pipeline. The combination of "species", "mode", and "age" uniquely determines a pipeline. However, you can directly specify pipeline subclasses from `sleap-roots.Pipeline` (as a string) to take precedence over the {species, mode, age} combination. The JSON file should maintain a consistent structure each time. Please ensure to include the following keys, and use null where any field is intentionally left empty:
    - `species`: valid species are "soybean", "canola", "pennycress", "arabidopsis", "rice".
    - `mode`: valid modes are "cylinder", "multiplant cylinder", "plate".
    - `age`: ages 2-14 days-old are allowed but are species-specific (see table).
    - `pipeline_class`: valid pipeline classes are "DicotPipeline", "YoungerMonocotPipeline", "OlderMonocotPipeline", "MultipleDicotPipeline",
    - Example:
        ```json
        {
            "species": "canola",
            "mode": "cylinder",
            "age": "7",
            "pipeline_class": "DicotPipeline"
        }
        ```

- **`output_dir`**: The directory where the output files will be saved.
    - **`traits_summary.csv`**: CSV with root traits from `sleap-roots`.


## Notes
- Ensure that the `input_dir` contains pipeline parameters, `predictions.csv` and the predictions as .slp files.
- Review and adjust the parameters in `pipeline_params.json` as needed to run the correct pipeline on the dataset.

## Badges
[![pipeline status](https://gitlab.com/salk-tm/sleap-roots-traits/badges/main/pipeline.svg)](https://gitlab.com/salk-tm/sleap-roots-traits/-/commits/main)

[![coverage report](https://gitlab.com/salk-tm/sleap-roots-traits/badges/main/coverage.svg)](https://gitlab.com/salk-tm/sleap-roots-traits/-/commits/main)

## Installation

**Make sure to have Docker Desktop running first**


You can pull the image if you don't have it built locally, or need to update the latest, with

```
docker pull registry.gitlab.com/salk-tm/sleap-roots-traits:latest
```

Then, to run the image interactively:

```
docker run -it registry.gitlab.com/salk-tm/sleap-roots-traits:latest bash
```

To run the image interactively with data mounted to the container, use the syntax

```
docker run -v /path/on/host:/path/in/container [other options] image_name [command]
```

For example: 

```
docker run -v D:\root_phenotyping\sleap-roots-pipeline\sleap-roots-traits\input:/workspace/input -v D:\root_phenotyping\sleap-roots-pipeline\sleap-roots-traits\output:/workspace/output -it registry.gitlab.com/salk-tm/sleap-roots-traits:latest bash
```

Then you can run the `main` function in the /workspace directory in the container using

```
python src/main.py ./input ./output
```

and check "/workspace/output" for the expected files (also available on the mounted host output folder).

This can all be run via the command line as well:

```
docker run -v D:\root_phenotyping\sleap-roots-pipeline\sleap-roots-traits\input:/workspace/input -v D:\root_phenotyping\sleap-roots-pipeline\sleap-roots-traits\output:/workspace/output -it registry.gitlab.com/salk-tm/sleap-roots-traits:latest python src/main.py ./input ./output
```

- The DockerFile installs the environment from `environment.yml` as the base environment on the container then copies "src" to the /workspace directory of the container. 


**Notes:**

- The `registry.gitlab.com` is the Docker registry where the images are pulled from. This is only used when pulling images from the cloud, and not necesary when building/running locally.
- `-it` ensures that you get an interactive terminal. The `i` stands for interactive, and `t` allocates a pseudo-TTY, which is what allows you to interact with the bash shell inside the container.
- The `-v` or `--volume` option mounts the specified directory with the same level of access as the directory has on the host.
- `bash` is the command that gets executed inside the container, which in this case is to start the bash shell.
- Order of operations is 1. Pull (if needed): Get a pre-built image from a registry. 2. Run: Start a container from an image.

## Support
contact Elizabeth at eberrigan@salk.edu

## Contributing
- Please make a new branch, starting with your name, with any changes, and request a reviewer before merging with the main branch since this container will be used by all HPI.
- Please document using the same conventions (docstrings for each function and class, typing-hints, informative comments).
- Tests are written in the pytest framework. Data used in the tests are defined as fixtures in "tests/fixtures/data.py" ("https://docs.pytest.org/en/6.2.x/reference.html#fixtures-api").

## Build
To build via automated CI, just push to `main`. See [`.gitlab-ci.yml`](.gitlab-ci.yml) for the runner definition.

To build locally for testing:

```
docker build --platform linux/amd64 --tag registry.gitlab.com/salk-tm/sleap-roots-traits:latest .
```

## `sleap-roots-pipeline`

This repo is part of a pipeline that is documented at this [hackmd](https://hackmd.io/@eberrigan/rJ32gON1C). 
- The run script for the entire pipeline is `run_pipeline.sh` in this repo since this is the last step of the pipeline. 

To run the entire pipeline use something like:

```
./run_pipeline.sh \
  /mnt/d/sleap-roots-pipeline/20240513_sleap-roots-pipeline-sequence/models_downloader_input \
  /mnt/d/sleap-roots-pipeline/20240513_sleap-roots-pipeline-sequence/models_downloader_output \
  /mnt/d/sleap-roots-pipeline/20240513_sleap-roots-pipeline-sequence/images_downloader_output \
  /mnt/d/sleap-roots-pipeline/20240513_sleap-roots-pipeline-sequence/sleap_roots_traits_input \
  /mnt/d/sleap-roots-pipeline/20240513_sleap-roots-pipeline-sequence/sleap_roots_traits_output
```

where the order matters and there are 5 arguments. See `run_pipeline.sh` for detailed documentation. 

**Tip**: When developing on a Windows machine use `dos2unix ./run_pipeline.sh` to reformat the run script befure running. 