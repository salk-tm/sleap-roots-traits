import json
import pandas as pd
import numpy as np
import logging
import sleap_roots

from pathlib import Path
from typing import List, Optional, Tuple, Union

from sleap_roots import (
    DicotPipeline,
    YoungerMonocotPipeline,
    OlderMonocotPipeline,
    MultipleDicotPipeline,
)


def get_params(input_dir: str) -> dict:
    """Read the parameters from the input directory from the `pipeline_params.json` file.

    Args:
        input_dir: Path to the input directory. This directory should contain
            a `pipeline_params.json` file which is used to identify the pipeline.

    Returns:
        dict: A dictionary containing the identifiying characteristics for a pipeline.

    """
    params = {}
    params_file = Path(input_dir) / "pipeline_params.json"
    if params_file.exists():
        with open(params_file, "r") as f:
            params = json.load(f)
        return params
    else:
        raise FileNotFoundError(f"pipeline_params.json not found in {input_dir}")


def get_species(params: dict, valid_species: List[str]) -> str:
    """Get the species name from the params dict.

    Args:
        params: A dictionary containing the species name.
        valid_species: A list of valid species names.

    Returns:
        str: The species name or None.

    """
    # get the species name
    species = params.get("species")
    # check if the species name is valid
    if species is not None and species not in valid_species:
        raise ValueError(f"Invalid species name: {species}")
    return species


def get_mode(params: dict, valid_modes: List[str]) -> str:
    """Get the mode from the params dict.

    Args:
        params: A dictionary containging the mode.
        valid_mode: A list of valid mode names for the type of image(s).
    Returns:
        str: The mode or None.

    """
    # get the mode
    mode = params.get("mode")
    # check if the mode is valid
    if mode is not None and mode not in valid_modes:
        raise ValueError(f"Invalid mode: {mode}")
    return mode


def get_age(params: dict, valid_age_ranges: List[str]) -> Optional[str]:
    """Get the age from the params dict.

    Args:
        params: A dictionary containing the age.
        valid_age_ranges: A list of valid ages or age ranges (as strings).
    Returns:
        Optional[str]: The age or None.

    """
    # get the age from params
    age = params.get("age")

    # Parse the valid ages into a set of individual ages
    valid_ages = set()
    for age_range in valid_age_ranges:
        if "," in age_range:
            ages = [a.strip() for a in age_range.split(",")]
            valid_ages.update(ages)
        elif "-" in age_range:
            start, end = map(int, map(str.strip, age_range.split("-")))
            valid_ages.update(str(a) for a in range(start, end + 1))
        else:
            valid_ages.add(age_range.strip())

    # Check if the age is valid
    if age is not None:
        age = age.strip()
        if age not in valid_ages:
            raise ValueError(f"Invalid age: {age}. Valid ages are {valid_ages}")
    return age


def get_pipeline_class(params: dict) -> str:
    """Get the pipeline class from the params dict.

    Args:
        params: A dictionary containing the pipeline class.

    Returns:
        str: The pipeline class or None."""
    # get pipeline name
    pipeline_name = params.get("pipeline_class")
    return pipeline_name


def determine_pipeline_class(
    pipeline_chooser: pd.DataFrame,
    species: Optional[str],
    mode: Optional[str],
    age: Optional[str],
    pipeline_class: Optional[str],
) -> Union[
    DicotPipeline, YoungerMonocotPipeline, OlderMonocotPipeline, MultipleDicotPipeline
]:
    """Determine the pipeline from sleap-roots to use for trait extraction.

    Args:
        pipeline_chooser: DataFrame for choosing pipeline.
        species: Species name from params.
        mode: Mode of images from params.
        age: Age of plants from params.
        pipeline_class: Pipeline class from params.
    Returns:
        chosen_pipeline_class: The pipeline class to use for trait extraction from sleap-roots.

    """
    pipeline_classes = {
        "DicotPipeline": DicotPipeline,
        "YoungerMonocotPipeline": YoungerMonocotPipeline,
        "OlderMonocotPipeline": OlderMonocotPipeline,
        "MultipleDicotPipeline": MultipleDicotPipeline,
    }

    # Check if the pipeline class is specified in the params
    if pipeline_class is not None:
        if pipeline_class not in pipeline_classes:
            raise ValueError(f"Invalid pipeline class: {pipeline_class}.")
        else:
            logging.info(f"Pipeline class specified in params: {pipeline_class}.")
            return pipeline_classes.get(pipeline_class)

    # If the `pipeline_class` is not provided in the params,
    # determine the pipeline based on the species, mode, and age
    # Filter the pipeline chooser dataframe
    filtered_df = pipeline_chooser[
        (pipeline_chooser["species"] == species)
        & (pipeline_chooser["mode"] == mode)
        & (pipeline_chooser["age"].str.contains(f"\\b{age}\\b"))
    ]

    # Check if there are any valid pipelines
    if filtered_df.empty:
        raise ValueError("No valid pipelines found.")

    # Drop rows with missing pipeline class
    valid_pipelines_df = filtered_df.dropna(subset=["pipeline_class"])

    # Check if there are any valid pipelines
    if valid_pipelines_df.empty:
        raise ValueError("No valid pipelines found.")

    # Check if there are multiple pipelines
    if len(valid_pipelines_df) > 1:
        raise ValueError(
            f"Multiple pipelines found: {valid_pipelines_df['pipeline_class'].tolist()}."
        )

    # Get the pipeline class
    chosen_pipeline_class = valid_pipelines_df["pipeline_class"].values[0]

    if chosen_pipeline_class not in pipeline_classes:
        raise ValueError(f"Invalid pipeline class: {chosen_pipeline_class}.")
    else:
        logging.info(f"Pipeline class determined: {chosen_pipeline_class}.")
        chosen_pipeline_class = pipeline_classes.get(chosen_pipeline_class)
        return chosen_pipeline_class
