import logging
import sys
import pandas as pd
import sleap_roots as sr

from typing import Optional
from pathlib import Path
from sleap_roots import (
    DicotPipeline,
    YoungerMonocotPipeline,
    OlderMonocotPipeline,
    MultipleDicotPipeline,
)

from src.log_util import setup_logging, StreamToLogger
from src.pipeline_chooser import (
    get_params,
    get_species,
    get_mode,
    get_pipeline_class,
    get_age,
    determine_pipeline_class,
)


def main(
    input_dir: str, output_dir: str, pipeline_class: Optional[str] = None
) -> pd.DataFrame:
    """Get the SLEAP predictions.

    Args:
        input_dir: Contains predictions per model/root type and predictions.csv.
        output_dir: Directory to save sleap-roots traits.

    Returns:
        pd.DataFrame: scan id, model id, root type, prediction path, traits.
    """
    # Make Path objects
    input_dir = Path(input_dir)
    output_dir = Path(output_dir)

    # Set up logging
    setup_logging(output_dir)
    logging.info(f"Using sleap-roots version {sr.__version__}.")

    # Read csv with path information
    predictions_csv = (
        input_dir / "predictions.csv"
    )  # scan id, plant_qr_code, model id, primary, lateral, crown
    logging.info(f"Reading predictions paths from {predictions_csv}.")
    predictions_df = pd.read_csv(predictions_csv)

    # Check which root predictions are available
    root_types = detect_root_types(predictions_df)

    if root_types:
        print("Root types present in the DataFrame:", root_types)
    else:
        print("No root types detected in the DataFrame.")
        return None

    # Find all .slp files in the input directory
    slp_paths = sr.find_all_slp_paths(input_dir)
    logging.info(f"Found {len(slp_paths)} .slp files in {input_dir}.")
    # Load the .slp files for each series
    all_series = sr.load_series_from_slps(slp_paths=slp_paths, h5s=False)
    logging.info(f"Loaded {len(all_series)} series from .slp files.")

    # Read the pipeline chooser table
    pipeline_chooser_path = Path(input_dir) / "pipeline_chooser_table.csv"
    pipeline_chooser = pd.read_csv(pipeline_chooser_path)
    logging.info(f"Reading pipeline chooser table from {pipeline_chooser_path}.")
    # Get the parameters from the pipeline_params.json file
    params = get_params(input_dir)
    logging.info(f"Using pipeline parameters: {params}.")

    # Fetch species, mode, age
    species = get_species(params, pipeline_chooser["species"].unique().tolist())
    mode = get_mode(params, pipeline_chooser["mode"].unique().tolist())
    age = get_age(params, pipeline_chooser["age"].unique().tolist())
    # Fetch pipeline class from params and sleap-roots classes
    pipeline_class = get_pipeline_class(params)

    # Determine the pipeline class using all of the params
    found_pipeline_class = determine_pipeline_class(
        pipeline_chooser, species, mode, age, pipeline_class
    )
    logging.info(f"Using pipeline class: {found_pipeline_class}.")

    # Construct path to output summary csv
    output_csv = output_dir / "traits_summary.csv"
    output_csv = output_csv.as_posix()

    if found_pipeline_class is MultipleDicotPipeline:
        logging.info("MultipleDicotPipeline detected. Skip for now.")
        return None

    else:
        # Initialize the pipeline
        pipeline = found_pipeline_class()
        # Get the traits for all series
        traits = pipeline.compute_batch_traits(all_series)

        # Merge the traits with the predictions DataFrame
        output_df = merge_traits(traits, predictions_df, output_csv)

        return output_df


def detect_root_types(df: pd.DataFrame) -> list[str]:
    """Detect the types of root predictions present in the DataFrame.

    Args:
        df: DataFrame containing root predictions for each root type. Dataframe is read
            from `predictions.csv`.

    Returns:
        list: A list of root types present in the DataFrame. Possible values are
            'primary', 'lateral', and 'crown'. If none of the root types are
            present, an empty list is returned.
    """
    root_types_present = []

    if "primary" in df.columns:
        root_types_present.append("primary")

    if "lateral" in df.columns:
        root_types_present.append("lateral")

    if "crown" in df.columns:
        root_types_present.append("crown")

    return root_types_present


def merge_traits(
    traits: pd.DataFrame, predictions_df: pd.DataFrame, output_csv: str
) -> pd.DataFrame:
    """Merge the traits DataFrame with the predictions DataFrame.

    Args:
        traits: DataFrame containing the traits for each series.
        predictions_df: DataFrame containing the prediction info for each root type.
        output_csv: Path to save the merged DataFrame.

    Returns:
        pd.DataFrame: Merged DataFrame containing the prediction info and traits.
    """
    # Extract the numeric part of 'plant_name' from sleap-roots output
    # and create the 'scan_id' column for merging
    traits["scan_id"] = traits["plant_name"].str.extract("(\d+)$")

    # Ensure 'scan_id' is of correct type for merging
    predictions_df["scan_id"] = predictions_df["scan_id"].astype(str)
    traits["scan_id"] = traits["scan_id"].astype(str)

    # Move 'scan_id' column to the first position without reordering other columns
    scan_id = traits.pop("scan_id")
    traits.insert(0, "scan_id", scan_id)

    # Merge the traits with the predictions DataFrame
    output_df = pd.merge(predictions_df, traits, on="scan_id", how="left")

    # Save the DataFrame to a csv file
    output_df.to_csv(output_csv, index=False)
    logging.info(f"Saved traits summary to {output_csv}.")

    return output_df


if __name__ == "__main__":
    input_dir = sys.argv[1]
    output_dir = sys.argv[2]
    main(input_dir, output_dir)
