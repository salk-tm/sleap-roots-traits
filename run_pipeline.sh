# Description: Run the entire pipeline
# Example: ./run_pipeline.sh <models_input_dir> <models_output_dir> <images_output_dir> <predict_output_dir> <final_output_dir>

set -e # Exit immediately if a command exits with a non-zero status
set -x # Print commands and their arguments as they are executed

# Check if correct number of arguments is provided
if [ "$#" -ne 5 ]; then
    echo "Usage: $0 <models_input_dir> <models_output_dir> <images_output_dir> <predict_output_dir> <final_output_dir>"
    exit 1
fi

# https://gitlab.com/salk-tm/models-downloader
# Input to `models-downloader` with zipped models containing model_chooser_table.xlsx, and model_params.json
MODELS_INPUT_DIR=$1
# Output of `models-downloader` with chosen models and model_paths.csv
# "model_path" column should be relative to this directory
# This is input to `sleap-roots-predict`
MODELS_OUTPUT_DIR=$2

# Output of `images-downloader` with images and scans.csv
# "scan_path" column should be relative to this directory
# This is input to `sleap-roots-predict`
IMAGES_OUTPUT_DIR=$3

# https://gitlab.com/salk-tm/sleap-roots-predict
# Output of `sleap-roots-predict` with predictions and predictions.csv
# prediction paths should be relative to this directory
# This is input to `sleap-roots-traits`
PREDICT_OUTPUT_DIR=$4

# Note: PREDICT_OUTPUT_DIR is the output directory for `sleap-roots-predict` and the 
# input directory for `sleap-roots-traits` so make sure to put 
# `pipeline_chooser_table.csv` and `pipeline_params.json`
# in PREDICT_OUTPUT_DIR before running the pipeline.

# https://gitlab.com/salk-tm/sleap-roots-traits
# Output of `sleap-roots-traits` with summarized traits `traits_summary.csv`
# This is the final output of the pipeline
FINAL_OUTPUT_DIR=$5


# Names or paths of Docker images
MODELS_DOWNLOADER=registry.gitlab.com/salk-tm/models-downloader:latest
SLEAP_ROOTS_PREDICT=registry.gitlab.com/salk-tm/sleap-roots-predict:latest
SLEAP_ROOTS_TRAITS=registry.gitlab.com/salk-tm/sleap-roots-traits:latest

# Check if Docker is installed
if ! command -v docker &> /dev/null; then
    echo "Error: Docker is not installed."
    exit 1
fi


# Make sure local images are up-to-date
echo "Pulling latest images..."
docker pull $MODELS_DOWNLOADER
docker pull $SLEAP_ROOTS_PREDICT
docker pull $SLEAP_ROOTS_TRAITS
echo "Images are up-to-date."


# Run `models-downloader` with host permissions on mounted volumes
echo "Running models-downloader..."
docker run --user root --privileged -v "$MODELS_INPUT_DIR":/workspace/input -v "$MODELS_OUTPUT_DIR":/workspace/output $MODELS_DOWNLOADER python /workspace/src/main.py /workspace/input /workspace/output

if [ $? -ne 0 ]; then
    echo "Error: Failed to run models-downloader."
    exit 1
fi

echo "models-downloader ran successfully. Check the output in $MODELS_OUTPUT_DIR."

# Run `sleap-roots-predict` with GPU support and host permissions on mounted volumes
echo "Running sleap-roots-predict with GPU support..."
docker run --user root --privileged -v "$IMAGES_OUTPUT_DIR":/workspace/images_input -v "$MODELS_OUTPUT_DIR":/workspace/models_input -v "$PREDICT_OUTPUT_DIR":/workspace/output --gpus all $SLEAP_ROOTS_PREDICT python /workspace/src/main.py /workspace/images_input /workspace/models_input /workspace/output

if [ $? -ne 0 ]; then
    echo "Error: Failed to run sleap-roots-predict."
    exit 1
fi

echo "sleap-roots-predict ran successfully. Check the output in $PREDICT_OUTPUT_DIR."

# Run `sleap-roots-traits` with host permissions on mounted volumes
echo "Running sleap-roots-traits..."
docker run --user root --privileged -v "$PREDICT_OUTPUT_DIR":/workspace/input -v "$FINAL_OUTPUT_DIR":/workspace/output $SLEAP_ROOTS_TRAITS python /workspace/src/main.py /workspace/input /workspace/output

if [ $? -ne 0 ]; then
    echo "Error: Failed to run sleap-roots-traits."
    exit 1
fi

echo "sleap-roots-traits ran successfully. Check the output in $FINAL_OUTPUT_DIR."

# Output image information for all images
echo "Image information for $MODELS_DOWNLOADER:" > "$FINAL_OUTPUT_DIR/image_info.txt"
docker inspect $MODELS_DOWNLOADER >> "$FINAL_OUTPUT_DIR/image_info.txt"

echo "" >> "$FINAL_OUTPUT_DIR/image_info.txt"  # Add a newline for separation
echo "Image information for $SLEAP_ROOTS_PREDICT:" >> "$FINAL_OUTPUT_DIR/image_info.txt"
docker inspect $SLEAP_ROOTS_PREDICT >> "$FINAL_OUTPUT_DIR/image_info.txt"

echo "" >> "$FINAL_OUTPUT_DIR/image_info.txt"  # Add a newline for separation
echo "Image information for $SLEAP_ROOTS_TRAITS:" >> "$FINAL_OUTPUT_DIR/image_info.txt"
docker inspect $SLEAP_ROOTS_TRAITS >> "$FINAL_OUTPUT_DIR/image_info.txt"

# Append GPU information to the image_info.txt file
echo "" >> "$FINAL_OUTPUT_DIR/image_info.txt"
echo "GPU information:" >> "$FINAL_OUTPUT_DIR/image_info.txt"
nvidia-smi >> "$FINAL_OUTPUT_DIR/image_info.txt"

echo "Pipeline completed successfully."

