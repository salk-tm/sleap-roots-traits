FROM mambaorg/micromamba:1.5.1

USER root
RUN apt-get update && apt-get install -y --no-install-recommends \
    build-essential \
    && rm -rf /var/lib/apt/lists/*
USER $MAMBA_USER

COPY --chown=$MAMBA_USER:$MAMBA_USER environment.yml /tmp/environment.yml
RUN micromamba install -y -n base -f /tmp/environment.yml && \
    micromamba clean --all --yes

WORKDIR /workspace
COPY ./src /workspace/src

# Set the PYTHONPATH environment variable to include the parent directory of the package
# This helps imports
ENV PYTHONPATH=/workspace:$PYTHONPATH

# Due to this "WARNING:matplotlib:Matplotlib created a temporary cache directory..."
# ..."it is highly recommended to set the MPLCONFIGDIR environment variable to a" 
# "writable directory, in particular to speed up the import of Matplotlib and to" 
# "better support multiprocessing."
# Create the directory with read/write permissions
RUN mkdir -p /workspace/temp/matplotlib && chmod a+rw /workspace/temp/matplotlib
# Set the MPLCONFIGDIR environment variable to the directory
ENV MPLCONFIGDIR=/workspace/temp/matplotlib
RUN echo $MPLCONFIGDIR